package com.example.myproductsapp.controllers;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myproductsapp.R;
import com.example.myproductsapp.models.DataStorage;
import com.example.myproductsapp.models.DbManager;
import com.example.myproductsapp.models.entities.Product;
import com.example.myproductsapp.views.CartActivity;
import com.example.myproductsapp.views.main_view_tools.RvAdapter;
import com.example.myproductsapp.views.MainActivity;

import java.util.ArrayList;

public class ControllerMainActivity {
    private MainActivity mainActivity;
    private DbManager db;

    private RecyclerView recycleViewProducts;

    public ControllerMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;

        DataStorage.Add("mainActivity",this.mainActivity);
        DataStorage.Add("context",this.mainActivity.getApplicationContext());

        db = DbManager.getInstance(this.mainActivity.getApplicationContext());

        recycleViewProducts = mainActivity.findViewById(R.id.recycleViewProducts);

        GridLayoutManager glm = new GridLayoutManager(mainActivity, 2);
        recycleViewProducts.setLayoutManager(glm);
    }

//    public void UpdateListViewProducts() {
//        ProductAdapter adapter = new ProductAdapter(products, mainActivity.getApplicationContext());
//
//        ListView listView = mainActivity.findViewById(R.id.listViewProducts);
//
//        listView.setAdapter(adapter);
//    }

    private void SetImagesIds(ArrayList<Product> products )
    {
        for (int i = 0; i < products.size(); i++)
        {
            Product product = products.get(i);

            int mainPicture = mainActivity.getResources().getIdentifier(product.getPicturePath(),"drawable", mainActivity.getPackageName());

            product.setPictureId(mainPicture);
        }
    }

    public void ConnectRvAdapterToProducts()
    {
        ArrayList<Product> products = db.GetTableProducts().getAll();

        SetImagesIds(products);

        RvAdapter adapter = new RvAdapter(products);
        recycleViewProducts.setAdapter(adapter);
    }

    public void InitializeButtonClick()
    {
        Button buttonStartSearch = mainActivity.findViewById(R.id.buttonStartSearch);
        buttonStartSearch.setOnClickListener(OnButtonStartSearchClickListener);

        Button buttonGoToCart = mainActivity.findViewById(R.id.buttonGoToCart);
        buttonGoToCart.setOnClickListener(OnButtonGoToCartClickListener);
    }


    private View.OnClickListener OnButtonStartSearchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            SearchMethod();
        }
    };

    private View.OnClickListener OnButtonGoToCartClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            Intent intent = new Intent(mainActivity, CartActivity.class);
            mainActivity.startActivity(intent);
        }
    };

    private void SearchMethod()
    {
        EditText editTextProductName = mainActivity.findViewById(R.id.editTextProductNameSearch);
        String partOfName = editTextProductName.getText().toString();

        ArrayList<Product> foundedProducts = db.GetTableProducts().GetByPartOfName(partOfName);

        SetImagesIds(foundedProducts);

        RvAdapter adapter = new RvAdapter(foundedProducts);
        recycleViewProducts.setAdapter(adapter);
    }
}

