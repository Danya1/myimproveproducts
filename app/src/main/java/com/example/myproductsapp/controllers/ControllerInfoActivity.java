package com.example.myproductsapp.controllers;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myproductsapp.R;
import com.example.myproductsapp.models.DataStorage;
import com.example.myproductsapp.models.DbManager;
import com.example.myproductsapp.models.entities.Product;
import com.example.myproductsapp.views.InfoActivity;

public class ControllerInfoActivity
{
    private InfoActivity infoActivity;
    private DbManager db;

    public ControllerInfoActivity(InfoActivity infoActivity)
    {
        this.infoActivity = infoActivity;
        Context context = (Context) DataStorage.Get("context");
        db = DbManager.getInstance(context);
    }

    public void InitializeButtonsClick()
    {
        Button buttonGoBackToSearch = infoActivity.findViewById(R.id.buttonGoBackToSearch);
        buttonGoBackToSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoActivity.finish();
            }
        });
    }

    public void FillInFields()
    {
        int productId = (int)DataStorage.Get("productId");
        DataStorage.Delete("productId");

        Product product = db.GetTableProducts().GetById(productId);

        Context context = (Context) DataStorage.Get("context");
        int pictureId = context.getResources().getIdentifier(product.getPicturePath(), "drawable", context.getPackageName());
        product.setPictureId(pictureId);

        ImageView imageOfProduct = infoActivity.findViewById(R.id.imageOfProduct);
        TextView textViewProductName = infoActivity.findViewById(R.id.textViewProductName);
        TextView textViewProductPrice = infoActivity.findViewById(R.id.textViewProductPrice);
        TextView textViewProductCountPurchases = infoActivity.findViewById(R.id.textViewProductCountPurchases);
        TextView textViewProductCountLeft = infoActivity.findViewById(R.id.textViewProductCountLeft);
        TextView textViewProductDescription = infoActivity.findViewById(R.id.textViewProductDescription);

        imageOfProduct.setBackgroundResource(product.getPictureId());
        textViewProductName.setText(product.getName());
        textViewProductPrice.setText("Цена: "+product.getPrice()+" руб.");
        textViewProductCountPurchases.setText("Всего куплено: "+product.getCountPurchase()+" шт.");
        textViewProductCountLeft.setText("Осталось на складе: "+product.getCountLeft()+" шт.");
        textViewProductDescription.setText(product.getDescription());
    }
}
