package com.example.myproductsapp.controllers;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myproductsapp.R;
import com.example.myproductsapp.models.DataStorage;
import com.example.myproductsapp.models.DbManager;
import com.example.myproductsapp.views.CartActivity;

public class ControllerCartActivity
{
    private CartActivity cartActivity;
    private DbManager db;

    private RecyclerView recycleViewProductsInCart;

    public ControllerCartActivity(CartActivity cartActivity) {
        this.cartActivity = cartActivity;

        DataStorage.Add("cartActivity", this.cartActivity);
        DataStorage.Add("context", this.cartActivity.getApplicationContext());

        db = DbManager.getInstance(this.cartActivity.getApplicationContext());

        recycleViewProductsInCart = cartActivity.findViewById(R.id.recycleViewProductsInCart);

        GridLayoutManager glm = new GridLayoutManager(cartActivity, 2);
        recycleViewProductsInCart.setLayoutManager(glm);
    }
}
