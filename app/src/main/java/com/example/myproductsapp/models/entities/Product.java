package com.example.myproductsapp.models.entities;

public class Product
{
    private int Id;
    private String Name;
    private String PicturePath;
    private int Price;
    private String Description;
    private int CountLeft;
    private int CountPurchase;

    private int PictureId;

    public Product(int id, String name, String picturePath, int price, String description, int countLeft, int countPurchase) {
        this.Id = id;
        this.Name = name;
        this.PicturePath = picturePath;
        this.Price = price;
        this.Description = description;
        this.CountLeft = countLeft;
        this.CountPurchase = countPurchase;

    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public int getPrice() {
        return Price;
    }

    public String getPicturePath() {
        return PicturePath;
    }

    public int getPictureId() {
        return PictureId;
    }

    public void setPictureId(int pictureId) {
        this.PictureId = pictureId;
    }

    public String getDescription() {
        return Description;
    }

    public int getCountLeft() {
        return CountLeft;
    }

    public int getCountPurchase() {
        return CountPurchase;
    }
}
