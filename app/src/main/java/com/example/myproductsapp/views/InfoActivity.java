package com.example.myproductsapp.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.myproductsapp.R;
import com.example.myproductsapp.controllers.ControllerInfoActivity;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        ControllerInfoActivity controller = new ControllerInfoActivity(this);
        controller.InitializeButtonsClick();
        controller.FillInFields();
    }
}