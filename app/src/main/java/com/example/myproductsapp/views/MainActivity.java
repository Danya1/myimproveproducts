package com.example.myproductsapp.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.myproductsapp.controllers.ControllerMainActivity;
import com.example.myproductsapp.R;

public class MainActivity extends AppCompatActivity {

    ControllerMainActivity controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        controller = new ControllerMainActivity(this);
        controller.InitializeButtonClick();
        controller.ConnectRvAdapterToProducts();
        //controller.UpdateListViewProducts();


    }
}