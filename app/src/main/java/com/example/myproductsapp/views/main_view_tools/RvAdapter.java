package com.example.myproductsapp.views.main_view_tools;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myproductsapp.R;
import com.example.myproductsapp.models.DataStorage;
import com.example.myproductsapp.models.entities.Product;
import com.example.myproductsapp.views.InfoActivity;
import com.example.myproductsapp.views.MainActivity;

import java.util.ArrayList;

public class RvAdapter extends RecyclerView.Adapter<RvAdapter.ProductViewHolder>
{

    public static class ProductViewHolder extends RecyclerView.ViewHolder {
        public CardView cv;
        public TextView productName;
        public TextView productPrice;
        public ImageView productPicture;
        public Button buttonGoToProductInfo;

        ProductViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv);
            productName = (TextView) itemView.findViewById(R.id.product_name);
            productPrice = (TextView) itemView.findViewById(R.id.product_price);
            productPicture = itemView.findViewById(R.id.product_picture);
            buttonGoToProductInfo = itemView.findViewById(R.id.buttonGoToProductInfo);
        }
    }

    private ArrayList<Product> products;

    public RvAdapter(ArrayList<Product> products)
    {
        this.products = products;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cv_item, viewGroup, false);
        ProductViewHolder pvh = new ProductViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder productViewHolder, int i)
    {
        productViewHolder.productName.setText(products.get(i).getName());
        productViewHolder.productPrice.setText(Integer.toString(products.get(i).getPrice()));
        productViewHolder.productPicture.setImageResource(products.get(i).getPictureId());

        productViewHolder.buttonGoToProductInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                DataStorage.Add("productId",products.get(i).getId());

                MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

                Intent intent = new Intent(mainActivity, InfoActivity.class);
                mainActivity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }
}
